const mongoose = require("mongoose");
const Order = new mongoose.Schema({
  // order_id: { type: Number },
  // buyer: { type: String },
  items: { type: String },
  amount: { type: Number },
  total_price: { type: Number },
});

module.exports = mongoose.model("Orders", Order);
