var express = require("express");
var router = express.Router();
const jwt = require("jsonwebtoken");

/* GET users listing. */
router.get("/", function (req, res, next) {
  res.send("respond with a resource");
});

router.post("/login", (req, res) => {
  let username = req.body.username;
  let rold = req.body.rold;
  let password = req.body.password;

  let payload = {
    username: username,
    rold: rold,
    password: password,
  };
  console.log(typeof payload.password);
  if (typeof payload.password != typeof "") {
    throw { message: "Pleass Input Password to String", status: 400 };
  }
  let token = jwt.sign(payload, process.env.secret_Key);
  res.send({
    token,
  });
});

let checkToken = (req, res, next) => {
  try {
    let token = req.headers.authorization.split("Bearer ")[1];
    let detoken = jwt.verify(token, process.env.secret_Key);
    req.detoken = detoken;
    next();
  } catch (err) {
    res.status(401).send({
      message: err.message,
    });
  }
};
router.get("/hello", checkToken, (req, res) => {
  res.send({
    message: `hello ${req.detoken.rold} ${req.detoken.username} ${req.detoken.password}`,
  });
});

module.exports = router;
