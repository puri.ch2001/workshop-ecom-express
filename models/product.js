const mongoose = require("mongoose");
const product = new mongoose.Schema({
  brand: { type: String },
  price: { type: Number },
  amount: { type: Number },
  detail: { type: Object },
});
module.exports = mongoose.model("products", product);
