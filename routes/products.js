const express = require("express");
const router = express.Router();
const productModel = require("../models/product");
const { default: mongoose } = require("mongoose");
console.log("productModel:", productModel);

router.post("/", async (req, res) => {
  try {
    let body = req.body;
    let new_product = new productModel({
      brand: body.brand,
      price: body.price,
      amount: body.amount,
      detail: body.detail,
    });
    let product = await new_product.save();

    return res.status(201).send({
      data: product,
      massage: "Creat Success",
    });
  } catch (err) {
    return res.status(err.status || 500).send({
      message: err.message,
    });
  }
});

////// GET ALL //////////

router.get("/", async function (req, res, next) {
  try {
    let product = await productModel.find(); //find() คือค้นหาทั้งหมด
    return res.status(200).send({
      data: product,
      message: "success",
      success: true,
    });
  } catch (error) {
    return res.status(500).send({
      message: "Sever Error",
      success: false,
    });
  }
});

/////// GET BY ID ///////////
router.get("/:id", async function (req, res, next) {
  try {
    let id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).send({
        message: "ID Invalid",
        success: false,
        error: ["id is not Object"],
      });
    }
    let product = await productModel.findById(id); //หาโดยใช้ id
    return res.status(200).send({
      data: product,
      message: "success",
      success: true,
    });
  } catch (error) {
    return res.status(500).send({
      message: "Sever Error",
      success: false,
    });
  }
});

////// UPDATE /////////
router.put("/:id", async function (req, res, next) {
  try {
    let id = req.params.id;
    let body = req.body;
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).send({
        message: "ID Invalid",
        success: false,
        error: ["id is not Object"],
      });
    }

    await productModel.updateOne(
      //เปลี่ยนแค่อันเดียว
      //{ _id: mongoose.Types.ObjectId(id) }, //หาข้อมูลโดยใช้ไอดี
      //{ $set: req.body }    //เปลี่ยนค่า

      { _id: id },
      {
        $set: {
          brand: body.brand,
          price: body.price,
          amount: body.amount,
          detail: body.detail,
        },
      }
    );
    let product = await productModel.findById(id);
    return res.status(201).send({
      data: product,
      message: "Update success",
      success: true,
    });
  } catch (error) {
    return res.status(500).send({
      message: "Sever Error",
      success: false,
    });
  }
});
//////// DELETE ///////
router.delete("/:id", async function (req, res, next) {
  try {
    let id = req.params.id;
    await productModel.deleteOne({ _id: id });
    let product = await productModel.find();
    return res.send({
      data: product,
      message: "Delete success",
    });
  } catch (err) {
    return res.status(500).send({
      message: err.massage,
    });
  }
});

module.exports = router;
