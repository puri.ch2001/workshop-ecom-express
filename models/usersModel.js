const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
  use_id: { type: Number },
  username: { type: String },
  password: { type: String },
  firstName: { type: String },
  lastName: { type: String },
  age: { type: Number },
  gender: { type: String },
});

module.exports = mongoose.model("Users", userSchema);
