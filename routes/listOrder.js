const express = require("express");
const router = express.Router();
const orderModel = require("../models/ordersModel"); //DB Model
const { default: mongoose } = require("mongoose");
const productModel = require("../models/product"); // ดึงมาจาก DB Product
const userModel = require("../models/usersModel"); // ดึงมาจาก DB User
//const productModel = require("../models/ordersModel");

/////////////////Create///////////////
router.post("/", async (req, res) => {
  try {
    let body = req.body;
    let new_order = new orderModel({
      // order_id: body.order_id, //ดึง user id มาจาก DB User
      // buyer: body.buyer, //ดึง user id มาจาก DB User
      items: body.items, //เป็น object เพื่อ put ลงใน DB product
      amount: body.amount,
      total_price: body.total_price,
    });
    //console.log("new: ", req.body);
    let dataOrder = await new_order.save();
    return res.status(201).send({
      data: dataOrder,
      massage: "Create Order Success",
    });
  } catch (err) {
    return res.status(err.status || 500).send({
      message: err.message,
    });
  }
});

///////////GET  ALL//////////////
router.get("/", async function (req, res, next) {
  try {
    let dataModel = await orderModel.find(); //find() คือค้นหาทั้งหมด
    console.log("findd: ", dataModel);
    return res.status(200).send({
      data: dataModel,
      message: "DB Order",
    });
  } catch (error) {
    return res.status(500).send({
      message: "Sever Error",
    });
  }
});

/////// GET BY ID ///////////
router.get("/:id", async function (req, res, next) {
  try {
    let id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).send({
        message: "ID Invalid",
        success: false,
        error: ["id is not Object"],
      });
    }
    console.log("hellll");
    let dataModel = await orderModel.findById(id); //หาโดยใช้ id
    return res.status(200).send({
      data: dataModel,
      message: "success",
      success: true,
    });
  } catch (error) {
    return res.status(500).send({
      message: "Sever Error",
      success: false,
    });
  }
});

////////// Create //////////////
router.get("/test", async function (req, res, next) {
  try {
    let dataUser = await userModel.find(); //find() คือค้นหาทั้งหมด orderModel
    let dataProduct = await productModel.find(); //find() คือค้นหาทั้งหมด productModel

    console.log("User: ", dataUser[0].use_id);
    console.log("Product: ", dataProduct[0].product_name);

    for (let i = 0; i < dataProduct.length; i++) {
      const product = dataProduct[i];
      console.log("PP: ", product);
      if (product.product_name === "Apple") {
        console.log("Total: ", product.amount);
      }
    }

    return res.status(200).send({
      data: dataProduct,
      message: "DB Product IN Order",
    });
  } catch (error) {
    return res.status(500).send({
      message: "Sever Error",
    });
  }
});

router.put("/:id", async function (req, res, next) {
  try {
    let id = req.params.id;
    let body = req.body;
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).send({
        message: "ID Invalid",
        success: false,
        error: ["id is not Object"],
      });
    }

    await orderModel.updateOne(
      { _id: id },
      {
        $set: {
          items: body.items,
          amount: body.amount,
          total_price: body.total_price,
        },
      }
    );
    let order = await orderModel.findById(id);
    return res.status(201).send({
      data: order,
      message: "Update success",
      success: true,
    });
  } catch (error) {
    return res.status(500).send({
      message: "Sever Error",
      success: false,
    });
  }
});

//////// DELETE ///////
router.delete("/:id", async function (req, res, next) {
  try {
    let id = req.params.id;
    await orderModel.deleteOne({ _id: id });
    let dataModel = await orderModel.find();
    return res.send({
      data: dataModel,
      message: "Delete success",
    });
  } catch (err) {
    return res.status(500).send({
      message: err.massage,
    });
  }
});

module.exports = router;
