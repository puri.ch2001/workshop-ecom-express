var express = require("express");
const { route } = require("./grade");
const multer = require("multer");
const storage = multer.diskStorage({
  destination: function (req, file, cd) {
    cd(null, "./public");
  },
  filename: function (req, file, cd) {
    cd(null, file.originalname);
  },
});

const upload = multer({ storage: storage });

var router = express.Router();
/* GET home page. */
router.get("/", function (req, res, next) {
  let query = req.query;
  console.log(query);
  res.send(query);
  // res.render("index", { title: "Express" });
});

// router.get("/:name/:age", function (req, res, next) {
//   let params = req.params;
//   console.log(params);
//   res.send(params);
// });

router.post("/", function (req, res, next) {
  try {
    let body = req.body;
    console.log(body);
    if (body.name === "") {
      throw { message: "กรุณาใส่ชื่อ", status: 400 };
    }

    return res.status(201).send({
      data: body,
      message: "Create Success",
    });
  } catch (err) {
    return res.status(err.status || 500).send({
      message: err.message,
    });
  }
});

router.put("/", function (req, res, next) {
  let body = req.body;
  console.log(body);
  res.send(body);
});

router.delete("/", function (req, res, next) {
  res.send("Delete");
});

router.post("/upload", upload.single("img"), (req, res) => {
  res.send({
    message: "upload success",
  });
});
///// โค้ด Upload ของพี่เขา ////
// const multer = require('multer')
// const storage = multer.diskStorage({
//   destination: function(req, file, cb) {
//     cb(null, "./public")
//   },
//   filename: function( req,file,cb) {
//     cb(null, file.originalname )
//   }
// })

// const upload = multer({storage:storage})

// router.post('/upload', upload.fields([{name:'img'},{name: 'file'}]), (req,res)=> {
//   res.send({
//     message: 'upload success'
//   })
// })
////////////////////////////////
module.exports = router;
