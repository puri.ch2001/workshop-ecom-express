var express = require("express");
var router = express.Router();

router.post("/", function (req, res, next) {
  try {
    let input = req.body.input;

    let output = {
      data: {
        subject: [],
        GPA: 0,
      },
      message: "success",
    };
    let totalScore = 0;
    for (let subject of input) {
      let grade = calculateGrade(subject.score);
      output.data.subject.push({
        subject: subject.subject,
        grade: grade[0],
      });
      console.log(grade[1]);
      totalScore += grade[1] * 3;
    }
    output.data.GPA = totalScore / (input.length * 3);
    return res.status(200).send(output);
  } catch (err) {
    return res.status(500).send({
      message: "Internal Server Error",
    });
  }
});

// ฟังก์ชันสำหรับคำนวณเกรด
function calculateGrade(score) {
  if (score >= 80) {
    return ["A", 4];
  } else if (score >= 75) {
    return ["B+", 3.5];
  } else if (score >= 70) {
    return ["B", 3];
  } else if (score >= 65) {
    return ["C+", 2.5];
  } else if (score >= 60) {
    return ["C", 2];
  } else if (score >= 55) {
    return ["D+", 1.5];
  } else if (score >= 50) {
    return ["D", 1];
  } else {
    return ["E", 0];
  }
}

module.exports = router;
