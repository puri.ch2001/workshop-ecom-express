const express = require("express");
const router = express.Router();
const { default: mongoose } = require("mongoose");
const userModel = require("../models/usersModel");

/////////// CREATE ////////////////////
router.post("/", async (req, res) => {
  try {
    let body = req.body;
    let new_user = new userModel({
      use_id: body.use_id,
      username: body.username,
      password: body.password,
      firstName: body.firstName,
      lastName: body.lastName,
      age: body.age,
      gender: body.gender,
    });
    //console.log("new: ", req.body);
    let dataUser = await new_user.save();
    return res.status(201).send({
      data: dataUser,
      massage: "Create User Success",
    });
  } catch (err) {
    return res.status(err.status || 500).send({
      message: err.message,
    });
  }
});

////////// GET ALL //////////////
router.get("/", async function (req, res, next) {
  try {
    let dataUser = await userModel.find(); //find() คือค้นหาทั้งหมด
    console.log("findd: ", dataUser);
    return res.status(200).send({
      data: dataUser,
      message: "Success",
    });
  } catch (error) {
    return res.status(500).send({
      message: "Sever Error",
    });
  }
});

////////// GET BY ID //////////////
router.get("/:id", async function (req, res, next) {
  try {
    let id = req.params.id;
    //console.log("ID: ", id);
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).send({
        message: "ID Invalid",
        error: ["id is not Object"],
      });
    }
    let dataUser = await userModel.findById(id); //หาโดยใช้ id

    return res.status(200).send({
      data: dataUser,
      message: "Get By ID Success",
    });
  } catch (error) {
    return res.status(500).send({
      message: "Sever Error",
      success: false,
    });
  }
});

///////////Delete////////////////
router.delete("/:id", async function (req, res, next) {
  try {
    let id = req.params.id;
    await userModel.deleteOne({ _id: id });
    let dataUser = await userModel.find();
    return res.send({
      data: dataUser,
      message: "Delete By ID Success",
    });
  } catch (err) {
    return res.status(500).send({
      message: err.massage,
    });
  }
});

////// UPDATE /////////
router.put("/:id", async function (req, res, next) {
  try {
    let id = req.params.id;
    let body = req.body;
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).send({
        message: "ID Invalid",
        error: ["id is not Object"],
      });
    }
    await userModel.updateOne(
      { _id: id },
      {
        $set: {
          use_id: body.use_id,
          username: body.username,
          password: body.password,
          firstName: body.firstName,
          lastName: body.lastName,
          age: body.age,
          gender: body.gender,
        },
      }
    );
    let dataUser = await userModel.findById(id);
    return res.status(201).send({
      data: dataUser,
      message: "Update success",
    });
  } catch (error) {
    return res.status(500).send({
      message: "Sever Error",
    });
  }
});
module.exports = router;
